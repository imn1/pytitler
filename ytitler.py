#!/usr/bin/env python3


import re
import aiohttp
import asyncio
import logging
import sys
from itertools import chain


logging.basicConfig(level=logging.DEBUG)


def parse_title(html):
    """
    Fetches title from HTML title tag
    from the given URL. Also removes the
    "- Youtube" suffix.

    :param str html: HTML the title is fetched from.
    :return: Parsed video title.
    :rtype: str
    """

    finds = re.search(r"<title>(.+)</title>", html)

    if finds:
        title = finds.group(1)
        parts = title.split("-")
        parts.pop()

        return "-".join(parts).strip()


def parse_duration(html):
    """
    Fetches duration from HTML. It's
    digged somewhere in inline Javascript.

    If not presented returns 0

    :param str html: HTML the title is fetched from.
    :return: Parsed duration or "0".
    :rtype: str
    """

    finds = re.search(r'lengthSeconds\\":\\"(\d+)\\"', html)

    if finds:
        duration = finds.group(1)

        return duration.strip()

    return "0"


def load_playlist(filename):
    """
    Loads URLs from a playlist file.
    Also removes duplicities.

    :param str filename: Playlist filename (path).
    :return: A list of URLs.
    :rtype: list
    """

    # Load file content.
    with open(filename, "r") as f:
        content = f.readlines()

    # Parse content.
    urls = []

    for l in content:
        if not l.startswith("#"):
            urls.append(l.strip())

    # Let's remove duplicities.
    uniq =  list(set(urls))

    if len(urls) != len(uniq):
        logging.info(f"Removed {len(urls) - len(uniq)} duplicities.")

    return uniq


def to_chunks(urls):
    """
    Splits URL list into smaller lists
    where each (a chunk) has 10 items.

    :param list urls: List of URLs.
    :return: List of lists (chunks).
    :rtype: list
    """

    in_bulk = 10

    for i in range(0, len(urls), in_bulk):
        yield urls[i:i + in_bulk]


def save_playlist(chunks, titles_durations):

    # Make chunks flat.
    urls = list(chain.from_iterable(chunks))

    with open(FILENAME, "w") as f:

        # Write headers.
        f.write("#EXTM3U\n")

        # Write URLs.
        for i, u in enumerate(urls):
            f.write(f"#EXTINF:{titles_durations[i][1]},{titles_durations[i][0]}\n")
            f.write(f"{u}\n")

        logging.info(f"Playlist written to {FILENAME}.")


async def fetch(chunks):

    async def fetch_one(session, url):
        """
        Scrape the given URL and parse out
        page title.

        :param aiohttp.ClientSession session: Aiohttp session.
        :param str url: URL to be scraped.
        :return: Parsed title.
        :rtype: str
        """

        async with session.get(url) as response:

            if 200 == response.status:
                title = parse_title(await response.text())
                duration = parse_duration(await response.text())

                logging.debug(f"URL {url} scraped.")

                return title, duration

    async def fetch_all(session, chunks):
        """
        Fetches all chunks.

        :param aiohttp.ClientSession session: Aiohttp session.
        :param list chunks: List of lists with URLs.
        :return: A list of fetched titles.
        :rtype: list
        """

        titles = []

        for i, ch in enumerate(chunks):

            logging.debug(f"{'-' * 10} CHUNK {i} {'-' * 10}")

            # Create a task for each URL and gather the bulk result into a list.
            titles.extend(await asyncio.gather(*[fetch_one(session, url) for url in ch]))

            # Security slow down.
            await asyncio.sleep(0.5)

        return titles

    # Parse titles.
    async with aiohttp.ClientSession() as session:
        titles_durations = await fetch_all(session, chunks)

    # Compile a new playlist.
    save_playlist(chunks, titles_durations)


if 2 == len(sys.argv):

    urls = load_playlist(sys.argv[1])
    urls = list(to_chunks(urls))
    asyncio.run(fetch(urls))

else:
    print("Please specify playlist filename.")

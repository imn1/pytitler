YTitler
=======
YouTube video title and duration async fetcher.

The title stands for YouTube (Y) titler (T).

What the program is for?
------------------------
If you use a video player (i.e. SMplayer) which can open
YouTube streams and keep your videos you wanna watch in
a playlist you probably has something like this:

.. image:: assets/playlist_1.png

We are missing something here. The player doesn't fetch the
video titles for us. Also the durations are 00:00 until you
actually start playng the video.

YTitler can help you with this:

.. image:: assets/playlist_2.png

How to use it?
--------------
First download the script of whole repo via `git clone`. Then:

::

   ./ytitler.py my_playlist.m3u

The file can also be plain text file as long as the program reads
only the lines not starting with ``#``. Each URL has to be on it's
own line.

Why do you even watch YouTube videos in a desktop player?
---------------------------------------------------------
Because YouTube playlist management sucks. And because by watching
a video in a player you get rid of ads. Yes, no ads at all.

Can I contribute or vote for new features?
------------------------------------------
Absolutely! Just open up new merge request or issue.
